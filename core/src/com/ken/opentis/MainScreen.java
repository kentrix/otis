package com.ken.opentis;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.ken.opentis.buttons.StartButton;
import com.ken.opentis.connection.Connection;
import com.ken.opentis.node.Editor;
import com.ken.opentis.node.Node;
import com.ken.opentis.node.NodeController;

import java.util.ArrayList;


/**
 * Created by you on 30/06/2016.
 */
public class MainScreen implements Screen {
    private Game game;
    private OrthographicCamera camera;
    private Stage stage;
    private BitmapFont font;
    private Skin skin;
    private static final int WIDTH = 1350;
    private static final int HEIGHT = 900;
    private ArrayList<Node> nodeList;
    private ArrayList<Connection> connectionList;
    private float width;

    public MainScreen(Game game) {
        this.game = game;
    }

    @Override
    public void show() {
        camera = new OrthographicCamera(WIDTH, HEIGHT);
        stage = new Stage(new ScreenViewport(camera));
        Gdx.input.setInputProcessor(stage);
        nodeList = new ArrayList<Node>();
        connectionList = new ArrayList<Connection>();


        skin = new Skin();


        FileHandle fileHandle = Gdx.files.internal("uiskin.json");
        FileHandle atlasFile = Gdx.files.internal("uiskin.atlas");
        if (atlasFile.exists()) {
            skin.addRegions(new TextureAtlas(atlasFile));
        }
        skin.load(fileHandle);



        populateNodeList();
        for(Node n: nodeList) {
            stage.addActor(n);
        }


        final NodeController nodeController = new NodeController(nodeList, connectionList);
        final Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                nodeController.nextState();
            }
        };

        Button startButton = new StartButton(skin);
        startButton.setPosition(20,20);
        startButton.setSize(40,40);
        startButton.addListener(new ClickListener(){

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                for(Node n : nodeList) {
                    n.disableEditor();
                    nodeController.init();
                }
                //Timer.schedule(task, 0, 0.5f);
                task.run();

                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public boolean mouseMoved(InputEvent event, float x, float y) {
                return super.mouseMoved(event, x, y);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }

        });

        stage.addActor(startButton);


    }

    @Override
    public void render(float delta) {
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void populateNodeList() {

        final float stageWidth, stageHeight;
        stageWidth = stage.getWidth();
        stageHeight = stage.getHeight();
        Gdx.app.debug("MainScr", "StageWidthHeight:" + stageWidth + " " + stageHeight);
        final float X_OFFSET = (float) (stageWidth * 0.25f);
    final float X_PADDING = (float) (stageWidth * 0.04f);
        final float NODE_WIDTH = (float) ((stageWidth * 0.75f - X_PADDING * 4) / 4f);
        final float Y_OFFSET = (float) (stageHeight * 0.08f);
        final float Y_PADDING = (float) (stageHeight * 0.04f);
        final float NODE_HEIGHT = (float) ((stageHeight * 0.76f) / 3f);

        Node node;
        Connection connection;

        for(int y = 0; y < 3; y++) {
            for(int x = 0; x < 4; x++) {
                node = new Node(skin,X_OFFSET + X_PADDING*x + NODE_WIDTH*x, Y_OFFSET + NODE_HEIGHT*y + Y_PADDING*y,
                        NODE_WIDTH, NODE_HEIGHT);
                nodeList.add(node);
            }
        }

        boolean even = true;
        for(int y = 0; y < 7; y++) {
            if (even) {
                even = !even;
                for (int x = 0; x < 4; x++) {
                    if (y == 0) {
                        connection = new Connection(skin, Connection.ConnectionType.D, "HOLDER");
                        connection.setSize(NODE_WIDTH, Y_PADDING);
                        connection.setPosition(X_OFFSET + NODE_WIDTH * x + X_PADDING * x, Y_OFFSET - Y_PADDING);
                        connectionList.add(connection);
                    } else if (y == 6) {
                        connection = new Connection(skin, Connection.ConnectionType.D, "HOLDER");
                        connection.setSize(NODE_WIDTH, Y_PADDING);
                        connection.setPosition(X_OFFSET + NODE_WIDTH * x + X_PADDING * x, Y_OFFSET + NODE_HEIGHT * 3 + Y_PADDING * 2);
                        connection.setValueRD(100);
                        connectionList.add(connection);
                    }
                    else {
                        connection = new Connection(skin, Connection.ConnectionType.UD);
                        connection.setSize(NODE_WIDTH,Y_PADDING);
                        connection.setPosition(X_OFFSET + NODE_WIDTH *x + X_PADDING * x, Y_OFFSET + NODE_HEIGHT * (int)(y/2) + Y_PADDING *(int)((y/2)-1));
                        connectionList.add(connection);
                    }
                }
            }
            else {
                even = !even;
                for (int x = 0; x < 3; x++) {
                    connection = new Connection(skin, Connection.ConnectionType.LR);
                    connection.setSize(X_PADDING, NODE_HEIGHT);
                    connection.setPosition(X_OFFSET + NODE_WIDTH*(x+1) + X_PADDING*x, (int)(y/2) * (Y_PADDING + NODE_HEIGHT) + Y_OFFSET);
                    connectionList.add(connection);
                }
            }
        }

        Gdx.app.debug("Main Scr", "Connection List size :" + connectionList.size());

        for(int y = 0; y < 3; y++) {
            for(int x = 0; x < 4; x++) {
                int c = (x+y*4)*2-y-x;
                node = nodeList.get(x+y*4);
                node.addConnection(Node.Side.DOWN, connectionList.get(c));
                node.addConnection(Node.Side.UP, connectionList.get(c+7));
                if(x != 0) {
                    node.addConnection(Node.Side.LEFT, connectionList.get(c+3));
                }
                if(x != 3) {
                    node.addConnection(Node.Side.RIGHT, connectionList.get(c+4));
                }
            }
        }
    }



}
