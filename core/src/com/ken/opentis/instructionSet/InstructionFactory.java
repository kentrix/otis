package com.ken.opentis.instructionSet;

import com.ken.opentis.node.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by you on 4/07/2016.
 */
public class InstructionFactory {
    public static Instruction instructionFromString(String str, Node node) {
        if(str.length() < 3) return null;
        String tstr = str.trim();
        String istr = tstr.substring(0, 3);
        if(istr.equalsIgnoreCase("MOV")) {
            List<String> stringList;
            stringList = nextWords(tstr.substring(3, tstr.length()).trim());
            if(stringList.size() != 2) {
                throw new RuntimeException();
            }
            return new Mov(getSide(stringList.get(0)), getSide(stringList.get(1)), node);
        }
        else if(istr.equalsIgnoreCase("ADD")) {
            String word = nextWords(tstr.substring(3, tstr.length()).trim()).get(0);
            try{
                int i = Integer.parseInt(word);
                return new Add(i, node);
            }
            catch (Exception e) {

            }
            return new Add(getSide(word), node);
        }
        else
            return null;
    }

    private static List<String> nextWords(String str) {

        Scanner scanner = new Scanner(str);
        scanner.useDelimiter("[^a-zA-Z0-9]+");

        char[] arr = str.toCharArray();
        List<String> stringList = new ArrayList<String>();

        while(scanner.hasNext()) {
            stringList.add(scanner.next());

        }

        return stringList;
        /*
        int i, j;
        for(i = 0; i < arr.length; i++) {
            if(!Character.isLetterOrDigit(arr[i]))
                break;
        }
        stringList.add(str.substring(0, i));
        if(i == arr.length - 1) return stringList;


        for(j = i;i < arr.length; i++) {
            if(!Character.isLetterOrDigit(arr[i]))
                break;
        }
        stringList.add(str.substring(j, i));
        return stringList;
*/
    }

    private static Node.Side getSide(String str) {
        if(str.equalsIgnoreCase("UP")) {
            return Node.Side.UP;
        }
        else if(str.equalsIgnoreCase("DOWN")) {
            return Node.Side.DOWN;
        }
        else if(str.equalsIgnoreCase("LEFT")) {
            return Node.Side.LEFT;
        }
        else if(str.equalsIgnoreCase("RIGHT")) {
            return Node.Side.RIGHT;
        }
        else if(str.equalsIgnoreCase("ANY")) {
            return Node.Side.ANY;
        }
        else if(str.equalsIgnoreCase("LAST")) {
            return Node.Side.LAST;
        }
        else if(str.equalsIgnoreCase("ACC")) {
            return Node.Side.ACC;
        }
        else {
            assert false;
            return null;
        }
    }

}
