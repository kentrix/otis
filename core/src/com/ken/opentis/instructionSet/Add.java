package com.ken.opentis.instructionSet;

import com.badlogic.gdx.Gdx;
import com.ken.opentis.connection.Connection;
import com.ken.opentis.connection.NoValueException;
import com.ken.opentis.node.Mode;
import com.ken.opentis.node.Node;
import com.ken.opentis.node.NodeController;

/**
 * Created by you on 7/07/2016.
 */
public class Add implements Instruction{

    private AddType addType;
    private int value;
    private Node.Side side;
    private Node node;

    private enum AddType{
        VALUE, SIDE
    }

    public Add(int value, Node node) {
        addType = AddType.VALUE;
        this.value = value;
        this.node = node;
    }

    public Add(Node.Side side, Node node) {
        addType = AddType.SIDE;
        this.side = side;
        this.node = node;
    }
    @Override
    public void actOnNode(NodeController nodeController) {
        switch(addType){
            case VALUE:
                node.incrementAcc(value);
                nodeController.getNodeRepFromNode(node).incrementProgrammeCounter();
                break;
            case SIDE:
                Connection connection = node.getConnectionMap().get(side);
                node.setModeType(Mode.modeType.READ);
                if(connection == null)
                    return;
                switch(side) {
                    //TODO
                    case UP:
                    case LEFT:
                        try {
                            node.incrementAcc(connection.readValueRD());
                            nodeController.getNodeRepFromNode(node).incrementProgrammeCounter();
                            Gdx.app.debug("ACC", "Read value success");
                        } catch (NoValueException e) {
                            e.printStackTrace();
                            return;
                        }

                        break;
                    case RIGHT:
                    case DOWN:
                        try {
                            node.incrementAcc(connection.readValueLU());
                            nodeController.getNodeRepFromNode(node).incrementProgrammeCounter();
                            Gdx.app.debug("ACC", "Read value success");
                        } catch (NoValueException e) {
                            e.printStackTrace();
                            return;
                        }
                        break;
                    default:
                        assert false;
                }
                break;
            default:
                assert false;
        }

    }

    @Override
    public int compareTo(Instruction o) {
        return 0;
    }
}
