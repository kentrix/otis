package com.ken.opentis.instructionSet;

import com.badlogic.gdx.Gdx;
import com.ken.opentis.connection.Connection;
import com.ken.opentis.connection.NoValueException;
import com.ken.opentis.node.Mode;
import com.ken.opentis.node.Node;
import com.ken.opentis.node.NodeController;

import static com.ken.opentis.instructionSet.Mov.ActionType.READ;
import static com.ken.opentis.instructionSet.Mov.ActionType.WRITE;

/**
 * Created by you on 5/07/2016.
 */
public class Mov implements Instruction {

    private Node.Side from, to;
    private int cache;
    private boolean hasCache = false;
    private Node node;
    private int value;
    ActionType actionType;


    public enum ActionType {
        READ, WRITE
    }

    public Mov(Node.Side from, Node.Side to, Node node) {
        actionType = READ;
        this.from = from;
        this.to = to;
        this.node = node;
    }

    public Mov(int value, Node.Side to, Node node) {
        actionType = READ;
        this.to = to;
        this.node = node;
        this.value = value;

    }

    @Override
    public void actOnNode(NodeController nodeController) {

        Connection connection;

        if(actionType == READ) {
            connection = node.getConnectionMap().get(from);
            node.setModeType(Mode.modeType.READ);
            if(connection == null && from != Node.Side.ACC)
                return;


            //READ
            switch(from) {
                case UP:
                case LEFT:
                    try {
                        cache = connection.readValueRD();
                        hasCache = true;
                        Gdx.app.debug("Mov", "Read value success");
                    } catch (NoValueException e) {
                            return;
                        }

                    break;
                case RIGHT:
                case DOWN:
                        try {
                            cache = connection.readValueLU();
                            hasCache = true;
                            Gdx.app.debug("Mov", "Read value success");
                        } catch (NoValueException e) {
                            return;
                        }
                    break;
                case ACC:
                    node.setModeType(Mode.modeType.RUN);
                    cache = node.getAccValue();
                    hasCache = true;
                    break;
                default:
                    assert false;
            }
            if(to == Node.Side.ACC) {
                nodeController.getNodeRepFromNode(node).incrementProgrammeCounter();
                node.setAccValue(cache);
                return;
            }
            actionType = WRITE;
        }
        else {
            connection = node.getConnectionMap().get(to);
            if(connection == null)
                return;
            switch(to) {
                case UP:
                case LEFT:
                    if(hasCache) {
                        node.setModeType(Mode.modeType.WRITE);
                        if(connection.writeValueLU(cache))
                            hasCache = false;
                        else
                            return;
                    }
                    break;
                case RIGHT:
                case DOWN:
                    if(hasCache) { //XXX There should always has cache here
                        node.setModeType(Mode.modeType.WRITE);
                        if(connection.writeValueRD(cache))
                            hasCache = false;
                        else
                            return;
                    }
                    break;
                default:
                    assert false;
            }
            nodeController.getNodeRepFromNode(node).incrementProgrammeCounter();
            actionType = READ;
        }
    }


    @Override
    public int compareTo(Instruction o) {
        if (o == null)
            throw new NullPointerException();
        if(o instanceof Mov) {
            if(((Mov) o).actionType == READ && actionType == WRITE) {
                return -1;
            }
            else if(((Mov) o).actionType == WRITE && actionType == READ) {
                return 1;
            }
        }
        return 0;
    }

}
