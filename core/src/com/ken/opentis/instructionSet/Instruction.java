package com.ken.opentis.instructionSet;

import com.ken.opentis.node.Node;
import com.ken.opentis.node.NodeController;

/**
 * Created by you on 1/07/2016.
 */
public interface Instruction extends Comparable<Instruction> {

    public abstract void actOnNode(NodeController nodeController);

}
