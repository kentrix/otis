package com.ken.opentis.font;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by you on 30/06/2016.
 */
public class StaticFont {

    private BitmapFont font;

    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("bit.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter params = new FreeTypeFontGenerator.FreeTypeFontParameter();
        params.size = 10;
        params.color = Color.WHITE;
        font = generator.generateFont(params);
        generator.dispose();

    }
    private static class StaticFontHolder {
        private static final StaticFont INSTANCE = new StaticFont();

    }

    public static StaticFont getInstance() {
        return StaticFontHolder.INSTANCE;
    }

    public final BitmapFont getFont() {
        return font;
}
}
