package com.ken.opentis.node;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.ken.opentis.connection.Connection;
import com.ken.opentis.instructionSet.Instruction;
import com.ken.opentis.instructionSet.InstructionFactory;

import java.util.*;

/**
 * Created by qhua948 on 26/06/2016.
 */
public class Node extends Widget{

    private Editor editor;
    private Acc acc;
    private Bak bak;
    private Last last;
    private Mode mode;
    private Idle idle;
    private Map<Side, Connection> connectionMap = new HashMap<Side, Connection>();


    private ShapeRenderer shape;
    private static final String TAG = "Node";
    private static final float COMPONENT_PADDING = 2f;

    public enum Side {
        UP, DOWN, LEFT, RIGHT, NA, ANY, LAST, ACC
    }

    public Node(Skin skin, float x, float y, float width, float height) {
        setPosition(x, y);
        setSize(width, height);
        shape = new ShapeRenderer();
        editor = new Editor(skin);
        float editorWidth = getWidth() - COMPONENT_PADDING * 3f - 50f;
        float smallComponentHeight = (getHeight() - 6f*COMPONENT_PADDING)/5f;

        //Gdx.app.debug(TAG, "getOriginX: " + getOriginX());
        //Gdx.app.debug(TAG, "getX: " + getX());
        editor.setPosition(COMPONENT_PADDING + getX(), COMPONENT_PADDING + getY());
        //Gdx.app.debug(TAG, "getWidth:" + getWidth());
        editor.setSize(editorWidth, getHeight() - COMPONENT_PADDING * 2);
        editor.setText("");
        editor.setHighlightRow(2);


        acc = new Acc(skin);
        acc.setPosition(editorWidth + COMPONENT_PADDING*2 + getX(), getY() + getHeight() - smallComponentHeight - COMPONENT_PADDING);
        acc.setSize(50, smallComponentHeight);
        acc.setValue(0);

        bak = new Bak();
        bak.setPosition(editorWidth + COMPONENT_PADDING*2 + getX(), getY() + getHeight() - (smallComponentHeight + COMPONENT_PADDING)*2);
        bak.setSize(50, smallComponentHeight);
        bak.setValue(123);

        last = new Last();
        last.setPosition(editorWidth + COMPONENT_PADDING*2 + getX(), getY() + getHeight() - (smallComponentHeight + COMPONENT_PADDING)*3);
        last.setSize(50, smallComponentHeight);
        last.setValue(Side.NA);


        mode = new Mode();
        mode.setPosition(editorWidth + COMPONENT_PADDING*2 + getX(), getY() + getHeight() - (smallComponentHeight + COMPONENT_PADDING)*4);
        mode.setSize(50, smallComponentHeight);
        mode.setValue(Mode.modeType.IDLE);


        idle = new Idle();
        idle.setPosition(editorWidth + COMPONENT_PADDING*2 + getX(), getY() + getHeight() - (smallComponentHeight + COMPONENT_PADDING)*5);
        idle.setSize(50, smallComponentHeight);
        idle.setValue("TEST");

    }


    @Override
    public void draw(Batch batch, float parentAlpha){
        //TODO check for actor
        getStage().addActor(editor);
        getStage().addActor(acc);
        getStage().addActor(bak);
        getStage().addActor(last);
        getStage().addActor(mode);
        getStage().addActor(idle);
        for(Connection c : connectionMap.values()) {
             getStage().addActor(c);
        }
        //Gdx.app.debug(TAG, "---------------------------");
        //for(Iterator iter = getStage().getActors().iterator();iter.hasNext();) {
        //    Gdx.app.debug(TAG, iter.next().toString());
        //}
        shape.begin(ShapeRenderer.ShapeType.Line);
        shape.setColor(Color.WHITE);
        shape.rect(getX(), getY(), getWidth(), getHeight());
        shape.end();
    }

    public void addConnection(Side side, Connection connection) {
        connectionMap.put(side, connection);

    }

    public List<Instruction> makeInstructions() {
        List<Instruction> instructionList = new ArrayList<Instruction>();
        for(Line l : editor.lines) {
            Instruction ins = InstructionFactory.instructionFromString(l.getCachedFullText(), this);
            if(ins != null) {
                instructionList.add(ins);
            }
        }
        return instructionList;
    }


    public Map<Side, Connection> getConnectionMap() {
        return connectionMap;
    }

    public void setEditorHightlightLine(int i) {
        editor.setHighlightRow(i);
    }

    public void disableEditor() {
        editor.disable();
    }

    public void setModeType(Mode.modeType type) {
        mode.setValue(type);
    }

    public void decrementAcc(int i) {
        acc.setValue(acc.getValue() - i);
    }

    public void incrementAcc(int i) {
        acc.setValue(acc.getValue() + i);
    }

    public void setAccValue(int i) {
        acc.setValue(i);
    }

    public int getAccValue() {
        return acc.getValue();
    }
}
