package com.ken.opentis.node;

/**
 * Created by you on 30/06/2016.
 */
public class Mode extends SmallComponent<Mode.modeType> {
    public enum modeType {
        IDLE, READ, WRITE, RUN
    }
    public Mode() {
        super();
    }
}
