package com.ken.opentis.node;

/**
 * Created by qhua948 on 26/06/2016.
 */


public class Element {
    public TISScanner.Kind kind;
    public String text;

    public Element(TISScanner.Kind k, String t) {
        text = t;
        kind = k;
    }

    public int countSpaces() {
        int sum = 0;

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == ' ') {
                sum += 1;
            } else {
                break;
            }
        }

        return sum;
    }
}
