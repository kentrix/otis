package com.ken.opentis.node;

import com.ken.opentis.connection.Connection;
import com.ken.opentis.instructionSet.Instruction;

import java.util.*;

/**
 * Created by you on 30/06/2016.
 */
public class NodeController {

    private static final String TAG = "NodeController";
    private boolean initalised = false;
    private final List<Node> nodeList;
    private final List<Connection> connectionList;
    private Map<Node, NodeRep> nodeListMap;

    public class NodeRep {
        private final List<Instruction> instructionList;
        private int programmeCounter = 0;

        private NodeRep(List<Instruction> instructions) {
            this.instructionList = instructions;
        }

        public void setProgrammeCounter(int programmeCounter) {
            if(programmeCounter < instructionList.size() && programmeCounter >= 0)
                this.programmeCounter = programmeCounter;
        }

        public void incrementProgrammeCounter() {
            this.programmeCounter++;
            if(programmeCounter >= instructionList.size()) {
                programmeCounter = 0;
            }
        }

        public Instruction getCurrentInstruction() {
            if(!instructionList.isEmpty())
                return instructionList.get(programmeCounter);
            else
                return null;
        }

        public void update(NodeController nodeController) {
            getCurrentInstruction().actOnNode(nodeController);
        }

        public int getCurrentProgrammeCounter() {
            return programmeCounter;
        }
    }

    public NodeController(List<Node> nodeList, List<Connection> connectionList) {
        this.nodeList = nodeList;
        this.connectionList = connectionList;
    }

    public void init() {
        if(!initalised) {
            nodeListMap = new HashMap<Node, NodeRep>();
            for (Node node : nodeList) {
                nodeListMap.put(node, new NodeRep(node.makeInstructions()));
            }
            initalised = true;
        }

    }

    public void nextState() {
        List<Instruction> tmp = new ArrayList<Instruction>();
        Instruction instruction;
        for(Connection c : connectionList) {
            c.update();
        }
        for(Node n : nodeList) {
            //instructionPriorityQueue.offer(nodeListMap.get(n).getCurrentInstruction());
            //nodeListMap.get(n).update(n);
            instruction = nodeListMap.get(n).getCurrentInstruction();
            if(instruction != null)
                tmp.add(nodeListMap.get(n).getCurrentInstruction());
            n.setEditorHightlightLine(nodeListMap.get(n).getCurrentProgrammeCounter() + 1);
        }
        Collections.sort(tmp);
        for(Instruction i : tmp) {
            i.actOnNode(this);
        }
    }

    public void disableAllNodes() {
        for(Node n : nodeList) {
            n.disableEditor();
        }
    }

    public NodeRep getNodeRepFromNode(Node node) {
        return nodeListMap.get(node);
    }
}
