package com.ken.opentis.node;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * Created by qhua948 on 26/06/2016.
 */
public class EditorStyle {
    public BitmapFont font;
    public Color fontColor, focusedFontColor, disabledFontColor;
    /**
     * Optional.
     */
    public Drawable background, focusedBackground, disabledBackground, cursor, selection;
    /**
     * Optional.
     */
    public BitmapFont messageFont;
    /**
     * Optional.
     */
    public Color messageFontColor;

    public EditorStyle() {
    }

    public EditorStyle(BitmapFont font, Color fontColor, Drawable cursor, Drawable selection, Drawable background) {
        this.background = background;
        this.cursor = cursor;
        this.font = font;
        this.fontColor = fontColor;
        this.selection = selection;
    }

    public EditorStyle(EditorStyle style) {
        this.messageFont = style.messageFont;
        if (style.messageFontColor != null)
            this.messageFontColor = new Color(style.messageFontColor);
        this.background = style.background;
        this.focusedBackground = style.focusedBackground;
        this.disabledBackground = style.disabledBackground;
        this.cursor = style.cursor;
        this.font = style.font;
        if (style.fontColor != null) this.fontColor = new Color(style.fontColor);
        if (style.focusedFontColor != null)
            this.focusedFontColor = new Color(style.focusedFontColor);
        if (style.disabledFontColor != null)
            this.disabledFontColor = new Color(style.disabledFontColor);
        this.selection = style.selection;
    }
}
