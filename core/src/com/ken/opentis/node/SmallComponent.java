package com.ken.opentis.node;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.ken.opentis.font.StaticFont;

/**
 * Created by you on 4/07/2016.
 */
public abstract class SmallComponent<T> extends Widget {
    protected final ShapeRenderer shapeRenderer;
    protected T value;

    public SmallComponent() {
        shapeRenderer = new ShapeRenderer();
    }

    public void setValue(final T val) {
        value = val;
    }

    public T getValue() {
        return value;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        BitmapFont font = StaticFont.getInstance().getFont();
        batch.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.rect(getX(), getY(), getWidth(), getHeight());
        shapeRenderer.end();
        batch.begin();
        final float OFFSET = getHeight() - font.getLineHeight()*2;
        font.setColor(new Color(0.9f, 0.9f, 0.9f, 0.8f));
        font.draw(batch, getClass().getSimpleName(), getX() + getWidth()/2 - font.getSpaceWidth()/2 * getClass().getSimpleName().length(), getY() + getHeight() - OFFSET + font.getLineHeight());
        font.setColor(Color.WHITE);
        font.draw(batch, String.valueOf(value) ,getX() +getWidth()/2 - font.getSpaceWidth()*String.valueOf(value).length()/2 , getY()+OFFSET);
    }
}
