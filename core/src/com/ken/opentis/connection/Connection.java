package com.ken.opentis.connection;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.ken.opentis.font.StaticFont;

/**
 * Created by you on 2/07/2016.
 */
public class Connection extends Widget {
    private static final String TAG = "Connection:";
    private static final float OFFSET = 7f;
    private static final BitmapFont font = StaticFont.getInstance().getFont();

    private final GlyphLayout glyphLayout = new GlyphLayout();
    private final ConnectionType TYPE;
    private final ShapeRenderer shapeRenderer;

    private String text = null;
    private int valueLU;
    private int valueRD;
    private boolean hasValueLU = false;
    private boolean hasValueRD = false;
    private boolean readLU = true;
    private boolean readRD = true;

    public enum ConnectionType {
        LR, UD, D
    }

    public Connection(final Skin skin, final ConnectionType type) {
        TYPE = type;
        shapeRenderer = new ShapeRenderer();
    }

    public Connection(final Skin skin, final ConnectionType type, final String text) throws RuntimeException{
        TYPE = type;
        if(TYPE != ConnectionType.D)
            throw new RuntimeException();
        this.text = text;
        shapeRenderer = new ShapeRenderer();
    }

    public boolean writeValueRD(int valueRD) {
        boolean ret = false;
        if(readRD) {
            this.valueRD = valueRD;
            hasValueRD = true;
            readRD = false;
            ret = true;
        }
        return ret;
    }

    public boolean writeValueLU(int valueLU) {
        boolean ret = false;
        if(readLU) {
            this.valueLU = valueLU;
            hasValueLU = true;
            readLU = false;
            ret = true;
        }
        return ret;
    }

    public void setValueRD(int valueRD) {
        this.valueRD = valueRD;
        readRD = false;
        hasValueRD = true;
    }

    public void setValueLU(int valueLU) {
        this.valueLU = valueLU;
        readLU = false;
        hasValueLU = true;
    }

    public int getValueRD() {
        return valueRD;
    }

    public int readValueLU() throws NoValueException {
        if(!readLU && hasValueLU) {
            readLU = true;
            return valueLU;
        }
        else throw new NoValueException();
    }

    public int readValueRD() throws NoValueException{
        if(!readRD && hasValueRD) {
            readRD = true;
            return valueRD;
        }
        else throw new NoValueException();
    }

    public boolean isHasValueLU() {
        return hasValueLU;
    }

    public boolean isHasValueRD() {
        return hasValueRD;
    }

    public boolean hasReadValueLU() {
        return readLU;
    }

    public boolean hasReadValueRD() {
        return readRD;
    }

    public int getValueLU() {
        return valueLU;
    }

    public void clearValueLU() {
        readLU = true;
        hasValueLU = false;
    }

    public void clearValueRD() {
        readRD = true;
        hasValueRD = false;
    }

    public void update() {
        if(readLU)
            clearValueLU();
        if(readRD)
            clearValueRD();
    }

    public void setText(String text) {
        this.text = text;
    }



    @Override
    public void draw(Batch batch, float parentAlpha) {
        final float WIDTH = getWidth();
        final float HEIGHT = getHeight();
        final float X = getX();
        final float Y = getY();
        double hyp;
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        batch.end();
        batch.begin();
        //Gdx.app.debug(TAG, String.valueOf(batch.isDrawing()));

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        Gdx.gl20.glLineWidth(1f);
        switch(TYPE) {
            case LR:
                hyp = (WIDTH * 0.25);
                /*
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT * 0.75), X + WIDTH - OFFSET,(float)(Y + HEIGHT * 0.75));
                shapeRenderer.line(X + WIDTH - OFFSET,(float)(Y + HEIGHT*0.75), X+WIDTH-OFFSET-(float)(Math.cos(0.523)*hyp),(float)(Y + HEIGHT * 0.75+(Math.sin(0.523) * hyp)));
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT * 0.25), X + WIDTH - OFFSET,(float)(Y + HEIGHT * 0.25));
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT*0.25), X+OFFSET+(float)(Math.cos(0.523)*hyp),(float)(Y+HEIGHT*0.25-(Math.sin(0.523) * hyp)));
                */
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT * 0.75), X + WIDTH - OFFSET,(float)(Y + HEIGHT * 0.75));
                shapeRenderer.line(X + WIDTH - OFFSET,(float)(Y + HEIGHT*0.25), X+WIDTH-OFFSET-(float)(Math.cos(0.523)*hyp),(float)(Y + HEIGHT * 0.25+(Math.sin(0.523) * hyp)));
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT * 0.25), X + WIDTH - OFFSET,(float)(Y + HEIGHT * 0.25));
                shapeRenderer.line(X + OFFSET,(float)(Y + HEIGHT*0.75), X+OFFSET+(float)(Math.cos(0.523)*hyp),(float)(Y+HEIGHT*0.75-(Math.sin(0.523) * hyp)));
                //glyphLayout.setText(font, String.valueOf(valueLU));
                if(hasValueLU)
                    //to the left
                    font.draw(batch, String.valueOf(valueLU), X + OFFSET, (float)(Y + HEIGHT *0.75) + font.getLineHeight());
                if(hasValueRD)
                    font.draw(batch, String.valueOf(valueRD), X + OFFSET + (float)(WIDTH * 0.25) , (float)(Y + HEIGHT *0.25) - font.getAscent());
                break;
            case UD:
                hyp = (HEIGHT * 0.25);
                shapeRenderer.line((float)(X+WIDTH*0.25),Y+OFFSET,(float)(X+WIDTH*0.25),Y+HEIGHT-OFFSET);
                shapeRenderer.line((float)(X+WIDTH*0.25),Y+HEIGHT-OFFSET, (float)(X+WIDTH*0.25-(Math.sin(0.523)*hyp)),(float)(Y+HEIGHT-OFFSET-(Math.cos(0.523) * hyp)));
                shapeRenderer.line((float)(X+WIDTH*0.75),Y+OFFSET,(float)(X+WIDTH*0.75),Y+HEIGHT-OFFSET);
                shapeRenderer.line((float)(X+WIDTH*0.75),Y+OFFSET, (float)(X+WIDTH*0.75-(Math.sin(0.523)*hyp)),(float)(Y+OFFSET+(Math.cos(0.523) * hyp)));
                //glyphLayout.setText(font, String.valueOf(valueLU));
                if(hasValueLU)
                    font.draw(batch, String.valueOf(valueLU), X + WIDTH*0.25f - OFFSET - font.getSpaceWidth()*String.valueOf(valueLU).length(), (float)(Y + HEIGHT/2));
                if(hasValueRD)
                    font.draw(batch, String.valueOf(valueRD), X + WIDTH*0.75f + OFFSET , (float)(Y+HEIGHT/2));
                break;
            case D:
                hyp = (HEIGHT * 0.25);
                shapeRenderer.line(X + WIDTH/2, Y + OFFSET, X + WIDTH/2, Y + HEIGHT - OFFSET);
                shapeRenderer.line((float)(X+WIDTH/2),Y+OFFSET, (float)(X+WIDTH/2-(Math.sin(0.523)*hyp)),(float)(Y+OFFSET+(Math.cos(0.523) * hyp)));
                if(hasValueRD)
                    font.draw(batch, String.valueOf(valueRD), X + WIDTH/2f + OFFSET, Y+HEIGHT/2 );
                if(text != null) {
                    font.draw(batch, text, X +WIDTH/2f - OFFSET - font.getSpaceWidth()*text.length(), Y+HEIGHT/2);
                }
                break;
            default:
                assert false;

        }
        batch.flush();
        shapeRenderer.end();

    }
}
